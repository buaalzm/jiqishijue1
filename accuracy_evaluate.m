function my_MSE = accuracy_evaluate(img,x,y)
%
% use mean square error to evaluate accuracy.
%
% @since 2019.10.12
% @param {double} [x] calculated x position.
% @param {double} [y] calculated y position.
% @return {double} [MSE] MSE value.
% 

    my_MSE = 0;
    for row = 1 : size(img, 1)
        for col = 1 : size(img, 2)
            my_MSE = my_MSE + ((row-y)^2+(col-x)^2)*double(img(row,col));
        end
    end

end
