function r = cal_spot_r(img)
%cal_spot_r - calculate light spot radius
%
% Syntax: r = cal_spot_r(img)
%
% input gray scale image 
% binarize input image with OTSU 
% estimate light spot radius
    level = graythresh(img);
    b_img = im2bw(img, level);
    % imshow(b_img);
    light_count = length(find(b_img==1));
    r = sqrt(light_count/pi);
end