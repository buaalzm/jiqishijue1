img = imread('�������.bmp');
[rows, cols] = size(img);

point_set = []; % x, y, z
for row = 1 : rows
    for col = 1 : cols
        if img(row, col) ~= 0&&img(row, col) ~= 255
            point_set(size(point_set,1)+1,:) = [row, col, img(row, col)]; 
        end
    end
end

A = []
for index = 1 : size(point_set, 1)
    A(index,:) = [point_set(index, 1)^2, point_set(index, 1), point_set(index, 2)^2, point_set(index, 2), 1]; % y^2, y, x^2, x, 1
    B(index) = log(point_set(index, 3));
end

para = eye(5)/(A'*A)*A'*B'; % (AT*A)^-1 *AT*b

% z = a*exp(-x1*(x-x0)^2)*exp(-y1*(y-y0)^2)
% ln(z) = ln(a) - x1*(x-x0)^2 -y1*(y-y0)^2

x1 = -para(3); % -x1*x^2
x0 = para(4)/2/x1; % 2*x1*x0*x
y1 = -para(1); % -y1*y^2
y0 = para(2)/2/y1; % 2*y1*y0*y
a = exp(para(5)+x1*x0^2+y1*y0^2); % ln(a) - x1*x0^2 - y1 * y0^2

[x,y] = meshgrid(1 : rows, 1 : cols);
z = a*exp(-x1*(x-x0).^2).*exp(-y1*(y-y0).^2);
surf(x,y,z);