img = imread('光斑中心.bmp');
subplot(1,3,1);
surf(img);
r = cal_spot_r(img);
sigma_ = r / sqrt(2);
gausFilter = fspecial('gaussian',[20 20],sigma_);   %高斯滤波
blur=imfilter(img, gausFilter,'replicate');        %对任意类型数组或多维图像进行滤波 
subplot(1,3,2);
surf(blur)

fuu = [1, -2, 1;1, -2, 1;1, -2, 1];
fvv = [1, 1, 1;-2, -2, -2;1, 1, 1];
fuv = [1, -1;-1, 1];

ruu = conv2(double(blur), double(fuu), 'same');
ruv = conv2(double(blur), double(fuv), 'same');
rvv = conv2(double(blur), double(fvv), 'same');

cuv = sigma_^4*(ruu.*rvv-ruv.^2);
subplot(1,3,3);
surf(cuv);

[row, col] = find(max(max(cuv)) == cuv);

fu = [-1, 1;-1, 1];
fv = [-1, -1;1, 1];
ru = conv2(double(blur), double(fu), 'same');
rv = conv2(double(blur), double(fv), 'same');

rx = ru(row, col);
ry = rv(row, col);
rxx = ruu(row, col);
rxy = ruv(row, col);
ryy = rvv(row, col);

s = (ry.*rxy-rx.*ryy)/(rxx.*ryy-rxy.^2);
t = (rx.*rxy-ry.*rxx)/(rxx.*ryy-rxy.^2);

display([row+s, col+t]);